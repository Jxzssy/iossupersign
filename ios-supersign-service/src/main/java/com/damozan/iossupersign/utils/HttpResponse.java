package com.damozan.iossupersign.utils;

import org.apache.http.Header;

import lombok.Data;

@Data
public class HttpResponse {

	public Header[] headers;
	
	public String body;
	
	public String reasonPhrase;
	
	public int statusCode;
	
	
}
