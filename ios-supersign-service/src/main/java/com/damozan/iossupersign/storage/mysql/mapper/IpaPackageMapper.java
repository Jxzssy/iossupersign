package com.damozan.iossupersign.storage.mysql.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.damozan.iossupersign.model.po.IpaPackagePO;

/**
 * @author Peter.Hong
 * @date 2019/12/11
 */
@Mapper
@Repository
public interface IpaPackageMapper {

    Page<IpaPackagePO> selectPage(Page<IpaPackagePO> pageParam, @Param("name")String name);

    IpaPackagePO selectByDownloadId(@Param("ipaDownloadId")String ipaDownloadId);

    int update(IpaPackagePO ipaPackagePO);

    int insert(IpaPackagePO ipaPackagePO);

    IpaPackagePO selectById(Long id);
}
