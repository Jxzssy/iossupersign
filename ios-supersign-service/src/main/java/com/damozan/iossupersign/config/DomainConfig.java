package com.damozan.iossupersign.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * Ami.TSai
 */
@Configuration
@Data
public class DomainConfig {

    @Value("${url.openapiUrlPath}")
    private String openapiUrlPath;
    
    @Value("${url.ipaUrlPath}")
    private String ipaUrlPath;
    
    @Value("${file.ipaUploadPath}")
    private String ipaUploadPath;
    
    
    @Value("${url.staticUrlPath}")
    private String staticUrlPath;

    @Value("${url.mobileConfigUrlPath}")
    private String mobileConfigUrlPath;
    
    
    

    
}