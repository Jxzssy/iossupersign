package com.damozan.iossupersign.config;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import nz.net.ultraq.thymeleaf.LayoutDialect;

/**
 * @author Peter.Hong
 * @date 2019/12/10
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Bean
    public LayoutDialect layoutDialect() {
        return new LayoutDialect();
    }

    @Bean  
    public MultipartConfigElement multipartConfigElement() {  
        MultipartConfigFactory factory = new MultipartConfigFactory();  
        //文件最大  
        factory.setMaxFileSize(DataSize.ofMegabytes(1024)); //KB,MB  
        /// 设置总上传数据总大小  
        factory.setMaxRequestSize(DataSize.ofMegabytes(1024));  
        
        return factory.createMultipartConfig();  
    } 
    
}
