package com.damozan.iossupersign.service.device;

import java.util.List;

import com.damozan.iossupersign.model.dto.AppleResultDTO;
import com.damozan.iossupersign.model.po.AccountDevicePO;
import com.damozan.iossupersign.model.po.DevicePO;

public interface DeviceBSService {
    boolean insertList(List<AppleResultDTO> deviceDTOS, Long appleId);

    DevicePO getDeviceByUdid(String udid);

    void insert(DevicePO devicePO);

    AccountDevicePO getAccountDeviceByUdid(String udid);
}
