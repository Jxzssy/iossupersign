package com.damozan.iossupersign.service.Ipapackage.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.damozan.iossupersign.model.po.IpaPackagePO;
import com.damozan.iossupersign.service.Ipapackage.IpaPackageService;
import com.damozan.iossupersign.storage.mysql.mapper.IpaPackageMapper;

/**
 * @author Peter.Hong
 * @date 2019/12/11
 */
@Service
public class IpaPackageServiceImpl implements IpaPackageService {

    private final IpaPackageMapper ipaPackageMapper;

    public IpaPackageServiceImpl(IpaPackageMapper ipaPackageMapper) {
        this.ipaPackageMapper = ipaPackageMapper;
    }

    @Override
    public int insertIpaPackage(IpaPackagePO ipaPackagePO) {
        return ipaPackageMapper.insert(ipaPackagePO);
    }

    @Override
    public void updateIpaPackage(IpaPackagePO ipaPackagePO) {
        ipaPackageMapper.update(ipaPackagePO);
    }

    @Override
    public Page<IpaPackagePO> getIpaPackagePage(Page<IpaPackagePO> page, String name) {
        return ipaPackageMapper.selectPage(page, name);
    }

    @Override
    public IpaPackagePO selectIpaPackageById(Long id) {
        return ipaPackageMapper.selectById(id);
    }

    @Override
    public IpaPackagePO selectByDownloadId(String downloadId) {
        return ipaPackageMapper.selectByDownloadId(downloadId);
    }


}
