package com.damozan.iossupersign.service.Ipapackage;

import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.damozan.iossupersign.model.bo.IpaPackageBO;
import com.damozan.iossupersign.model.po.IpaPackagePO;

/**
 * @author Peter.Hong
 * @date 2019/12/11
 */
public interface IpaPackageBSService {

    void uploadIpa(IpaPackageBO ipaPackageBO);

    Page<IpaPackagePO> selectIpaByName(@NotNull Integer currentPage, String name);

    IpaPackagePO selectIpaByDownloadId(@NotNull String downloadId);

}
