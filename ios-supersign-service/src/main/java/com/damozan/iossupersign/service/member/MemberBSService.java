package com.damozan.iossupersign.service.member;

import java.util.List;
import java.util.Optional;

import com.damozan.iossupersign.model.po.MemberPO;

/**
 * @author Peter.Hong
 * @date 2019/12/10
 */
public interface MemberBSService {

    Optional<MemberPO> getMemberByAccount(String account);

    List<MemberPO> selectMemberByCondition(MemberPO memberPO);

    void create(MemberPO memberPO);

    void update(MemberPO memberPO);
}
