package com.damozan.iossupersign.service.device.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Service;

import com.damozan.iossupersign.assembly.DeviceAssembly;
import com.damozan.iossupersign.model.dto.AppleResultDTO;
import com.damozan.iossupersign.model.po.AccountDevicePO;
import com.damozan.iossupersign.model.po.DevicePO;
import com.damozan.iossupersign.service.device.DeviceBSService;
import com.damozan.iossupersign.service.device.DeviceService;

@Service
public class DeviceBSServiceImpl implements DeviceBSService {

    private final DeviceService deviceService;

    private final DeviceAssembly deviceAssembly;

    public DeviceBSServiceImpl(DeviceService deviceService, DeviceAssembly deviceAssembly) {
        this.deviceService = deviceService;
        this.deviceAssembly = deviceAssembly;
    }

    @Override
    public boolean insertList(List<AppleResultDTO> deviceDTOS, Long appleId){
        List<DevicePO> devicePOS = deviceAssembly.assembleDevicePOByAppleDeviceDTO(deviceDTOS,appleId);
       if(devicePOS!=null && devicePOS.size()>0){
    	   for(DevicePO dev :devicePOS){
    		   if(getAccountDeviceByUdid(dev.getUdid()) == null){
    			   this.insert(dev);
    		   }
    	   }
       } 
        return true;
    }

    @Override
    public DevicePO getDeviceByUdid(String udid){
       return deviceService.getDeviceByUdid(udid);
    }

    @Override
    public void insert(DevicePO devicePO){
        devicePO.setCreateTime(LocalDate.now());
        deviceService.insert(devicePO);
    }

    @Override
    public AccountDevicePO getAccountDeviceByUdid(String udid) {
        return deviceService.getAccountDeviceByUdid(udid);
    }

}
