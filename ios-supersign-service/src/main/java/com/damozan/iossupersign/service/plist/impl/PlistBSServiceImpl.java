package com.damozan.iossupersign.service.plist.impl;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.damozan.iossupersign.config.DomainConfig;
import com.damozan.iossupersign.controller.IpaController;
import com.damozan.iossupersign.model.po.AccountDevicePO;
import com.damozan.iossupersign.model.po.IpaPackagePO;
import com.damozan.iossupersign.service.Ipapackage.IpaPackageBSService;
import com.damozan.iossupersign.service.device.DeviceBSService;
import com.damozan.iossupersign.service.plist.PlistBService;
import com.damozan.iossupersign.service.profile.ProfileBSService;
import com.damozan.iossupersign.utils.FileUtils;
import com.damozan.iossupersign.utils.IosUrlUtils;
import com.damozan.iossupersign.utils.PropUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Peter.Hong
 * @date 2019/12/14
 */
@Slf4j
@Service
public class PlistBSServiceImpl implements PlistBService {

    private final String plistTemplatePath = "plistTemplate";

    private String plistTemplate;

    private final IpaPackageBSService ipaPackageBSService;

    private final DeviceBSService deviceBSService;

    private final ProfileBSService profileBSService;

    private final DomainConfig domainConfig;

    @Autowired
    public PlistBSServiceImpl(IpaPackageBSService ipaPackageBSService, DeviceBSService deviceBSService, ProfileBSService profileBSService, DomainConfig domainConfig) {
        this.ipaPackageBSService = ipaPackageBSService;
        this.deviceBSService = deviceBSService;
        this.profileBSService = profileBSService;
        this.domainConfig = domainConfig;
    }

    @PostConstruct
    public void initPlistTemplate() throws URISyntaxException, IOException {
        plistTemplate =  new String(FileUtils.readByte("/opt/wwwroot/supersign/"+plistTemplatePath) );
    }

    @Override
    public String getPlist(String downloadId, String udid) {
    	   
        log.info("=========getPlist==========");
        
        // 拿取原版ipa
        IpaPackagePO ipaPackagePO = ipaPackageBSService.selectIpaByDownloadId(downloadId);

        // 利用udid拿取appleAccount資訊
        AccountDevicePO accountDevicePO = deviceBSService.getAccountDeviceByUdid(udid);

        // 拿取新的profile mobileprovision (重簽需要)
        String profilePath = profileBSService.createNewProfile(accountDevicePO);
        // 重簽回傳下載url
     
        log.info("profilePath:{}",profilePath);
        
        String p12Path = "";
        if(StringUtils.isNotEmpty(accountDevicePO.getP12())) {
        	p12Path = accountDevicePO.getP12().replace(".p12", ""); 
        }
        
        String targetIpa = domainConfig.getIpaUploadPath()+""+downloadId+"_resign/"+udid+".ipa" ;
        FileUtils.createFile(targetIpa);
        
        String ipaUrl = IosUrlUtils.getIpaUrl(domainConfig.getIpaUrlPath(), downloadId,p12Path,profilePath,udid,targetIpa,ipaPackagePO.getLink());
         
        log.info("ipaUrl:{}",ipaUrl);
        
        // 組plist
        // 需拿重簽過後的ipaLink
        String plist = plistTemplate
//                .replace("@@IpaLink", Objects.toString(ipaPackagePO.getLink(),""))
                .replace("@@IpaLink", ipaUrl)
                .replace("@@BundleIdentifier", Objects.toString(ipaPackagePO.getBundleIdentifier(),""))
                .replace("@@BundleVersion", Objects.toString(ipaPackagePO.getBuildVersion(),""))
                .replace("@@Name", Objects.toString(ipaPackagePO.getName(),""));
        
        log.info("plist:{}",plist);
        
        
        log.info("=========getPlist end==========");
        return plist;
    }
}
