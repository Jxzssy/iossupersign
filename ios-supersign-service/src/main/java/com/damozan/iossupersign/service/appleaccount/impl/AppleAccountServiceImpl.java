package com.damozan.iossupersign.service.appleaccount.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.damozan.iossupersign.model.po.AppleAccountPO;
import com.damozan.iossupersign.service.appleaccount.AppleAccountService;
import com.damozan.iossupersign.storage.mysql.mapper.AppleAccountMapper;

@Service
public class AppleAccountServiceImpl implements AppleAccountService {

    private final AppleAccountMapper appleAccountMapper;

    public AppleAccountServiceImpl(AppleAccountMapper appleAccountMapper) {
        this.appleAccountMapper = appleAccountMapper;
    }

    @Override
    public int insert(AppleAccountPO appleAccountPO) {
        return appleAccountMapper.insert(appleAccountPO);
    }


    @Override
    public AppleAccountPO getAccountByAccount(AppleAccountPO appleAccountPO) {
        return appleAccountMapper.getAppleAccountByAccount(appleAccountPO.getAccount());
    }

    @Override
    public Page<AppleAccountPO> selectAppleAccountByCondition(Page<AppleAccountPO> page, AppleAccountPO appleAccountPO) {
       return appleAccountMapper.selectAppleAccountByCondition(page , appleAccountPO);
    }

    @Override
    public AppleAccountPO getAccountById(Long id) {
        return appleAccountMapper.getAccountById(id);
    }

    @Override
    public List<AppleAccountPO> selectEnableAppleAccounts(Integer deviceLimit , Integer sizeLimit) {
       return appleAccountMapper.selectEnableAppleAccounts(deviceLimit , sizeLimit);
    }

    @Override
    public void updateAccountDeviceCount(String account, Integer deviceCount) {
        appleAccountMapper.updateAccountDeviceCount(account,deviceCount);
    }

    @Override
    public void updateAccountP12Path(String account, String p12Path) {
        appleAccountMapper.updateAccountP12Path(account, p12Path);
    }
}