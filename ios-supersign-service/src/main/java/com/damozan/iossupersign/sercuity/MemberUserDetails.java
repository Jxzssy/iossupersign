package com.damozan.iossupersign.sercuity;


import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Data;

/**
 * @author Peter.Hong
 * @date 2019/12/10
 */
@Data
public class MemberUserDetails extends User {

    public MemberUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

}
