package com.damozan.iossupersign.model.dto;

import java.io.Serializable;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AppleResultDTO implements Serializable {
 
	private static final long serialVersionUID = 1L;

	private String id;
    
    private String type;
 
    private Map<String,Object> attributes;

}
