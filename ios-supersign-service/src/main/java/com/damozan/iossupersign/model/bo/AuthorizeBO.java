package com.damozan.iossupersign.model.bo;

import com.damozan.iossupersign.model.po.AppleAccountPO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AuthorizeBO {

    private String csr;

    private String p8;

    private String iss;

    private String kid;

    public AuthorizeBO(AppleAccountPO appleAccountPO){
        this.iss = appleAccountPO.getIssuerId();
        this.kid = appleAccountPO.getKid();
        this.p8 = appleAccountPO.getP8();
    }

}
