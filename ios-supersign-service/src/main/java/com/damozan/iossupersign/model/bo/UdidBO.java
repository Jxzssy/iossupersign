package com.damozan.iossupersign.model.bo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UdidBO implements Serializable {

    private String payloadOrganization;

    private String payloadDisplayName;

    private String payloadUUID;
}
